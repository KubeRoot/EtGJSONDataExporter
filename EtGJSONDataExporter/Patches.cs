﻿using System;
using System.Collections.Generic;
using System.Text;
using MonoMod;

namespace EtGJSONDataExporter.Patches
{
    [MonoModPatch("global::GameStatsManager")]
    internal class patch_GameStatsManager : GameStatsManager
    {
        public static extern void orig_Load();
        public static void Load()
        {
            //UnityEngine.Debug.Log("Intercepted GameStatsManager");
            orig_Load();
            Main.DoStuff();
        }

    }

    /*
    [MonoModPatch("global::JournalEntry")]
    internal class patch_JournalEntry : JournalEntry
    {
        public extern JournalEntry orig_Clone();
        public JournalEntry Clone()
        {
            UnityEngine.Debug.Log($"JournalEntry Cloning {PrimaryDisplayName}");
            return orig_Clone();
        }
    }
    */
}
