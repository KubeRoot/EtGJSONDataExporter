﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EtGJSONDataExporter
{
    public partial class DatabaseHandler
    {
        class PickupObjectEntry : ParsedEntry
        {
            public string Quality;
            public int Price;
            public bool CanBeDropped;
            public bool CanBeSold;
            public int PickupObjectID;

            public override string EntryType
            {
                get => "PickupObject";
            }

            protected override void SetupInwards()
            {
                if (Name == null) Name = (origvalue as PickupObject).DisplayName;
                base.SetupInwards();
            }

            public PickupObjectEntry(PickupObject item) : base(item)
            {
                Quality = item.quality.ToString();
                Price = item.PurchasePrice;
                CanBeDropped = item.CanBeDropped;
                CanBeSold = item.CanBeSold;
                maybeuniquename = item.name;
                PickupObjectID = item.PickupObjectId;

                EncounterTrackable encounter = item.GetComponent<EncounterTrackable>();

                if (encounter != null)
                {
                    SetupEncounterData(EncounterDatabase.GetEntry(encounter.EncounterGuid));
                }
            }
        }
    }
}
