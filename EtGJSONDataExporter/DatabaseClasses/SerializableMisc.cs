﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace EtGJSONDataExporter
{
    public partial class DatabaseHandler
    {

        public struct SerializableVector2
        {
            public float x, y;

            public static SerializableVector2 FromVector2(Vector2 v)
            {
                return new SerializableVector2 { x = v.x, y = v.y };
            }
        }

        public class SerializablePrototypeDungeonRoom
        {
            public PrototypeDungeonRoom.RoomCategory category;
            public int RequiredCurseLevel;
            public int RoomId;
            public PrototypeDungeonRoom.RoomBossSubCategory subCategoryBoss;
            public PrototypeDungeonRoom.RoomNormalSubCategory subCategoryNormal;
            public PrototypeDungeonRoom.RoomSecretSubCategory subCategorySecret;
            public PrototypeDungeonRoom.RoomSpecialSubCategory subCategorySpecial;
            public string CustomMusicEvent;
            public bool UseCustomMusic;
            public int Height;
            public int Width;

            public SerializablePrototypeDungeonRoom(PrototypeDungeonRoom orig)
            {
                category = orig.category;
                RequiredCurseLevel = orig.RequiredCurseLevel;
                RoomId = orig.RoomId;
                subCategoryBoss = orig.subCategoryBoss;
                subCategoryNormal = orig.subCategoryNormal;
                subCategorySecret = orig.subCategorySecret;
                subCategorySpecial = orig.subCategorySpecial;
                CustomMusicEvent = orig.CustomMusicEvent;
                UseCustomMusic = orig.UseCustomMusic;
                Height = orig.Height;
                Width = orig.Width;
            }
        }

        public class SerializablePrerequisite
        {
            public DungeonPrerequisite.PrerequisiteOperation prerequisiteOperation;
            public TrackedStats statToCheck;
            public TrackedMaximums maxToCheck;
            public float comparisonValue;
            public bool useSessionStatValue;
            public string encounteredObjectGuid;
            public int requiredNumberOfEncounters;
            public PlayableCharacters requiredCharacter;
            public bool requireCharacter;
            public GlobalDungeonData.ValidTilesets requiredTileset;
            public bool requireTileset;
            public GungeonFlags saveFlagToCheck;
            public bool requireFlag;
            public bool requireDemoMode;
            public DungeonPrerequisite.PrerequisiteType prerequisiteType;

            public SerializablePrototypeDungeonRoom encounteredRoom;

            public SerializablePrerequisite(DungeonPrerequisite orig)
            {
                prerequisiteOperation = orig.prerequisiteOperation;
                statToCheck = orig.statToCheck;
                maxToCheck = orig.maxToCheck;
                comparisonValue = orig.comparisonValue;
                useSessionStatValue = orig.useSessionStatValue;
                encounteredObjectGuid = orig.encounteredObjectGuid;
                requiredNumberOfEncounters = orig.requiredNumberOfEncounters;
                requiredCharacter = orig.requiredCharacter;
                requireCharacter = orig.requireCharacter;
                requiredTileset = orig.requiredTileset;
                requireTileset = orig.requireTileset;
                saveFlagToCheck = orig.saveFlagToCheck;
                requireFlag = orig.requireFlag;
                requireDemoMode = orig.requireDemoMode;
                prerequisiteType = orig.prerequisiteType;

                if (encounteredRoom != null)
                    encounteredRoom = new SerializablePrototypeDungeonRoom(orig.encounteredRoom);
            }
        }
    }
}
