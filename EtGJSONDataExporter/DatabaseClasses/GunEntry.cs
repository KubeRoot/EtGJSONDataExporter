﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EtGJSONDataExporter
{
    public partial class DatabaseHandler
    {
        public class SerializableProjectile
        {
            public bool SpawnedFromOtherPlayerProjectile;
            public float PlayerProjectileSourceGameTimeslice;
            public BulletScriptSettings BulletScriptSettings;
            public CoreDamageTypes damageTypes;
            public bool allowSelfShooting;
            public bool collidesWithPlayer;
            public bool collidesWithProjectiles;
            public bool collidesOnlyWithPlayerProjectiles;
            public int projectileHitHealth;
            public bool collidesWithEnemies;
            public bool shouldRotate;
            public bool shouldFlipVertically;
            public bool shouldFlipHorizontally;
            public bool ignoreDamageCaps;
            public ProjectileData baseData;
            public bool AppliesPoison;
            public float PoisonApplyChance;
            public GameActorHealthEffect healthEffect;
            public bool AppliesSpeedModifier;
            public float SpeedApplyChance;
            public GameActorSpeedEffect speedEffect;
            public bool AppliesCharm;
            public float CharmApplyChance;
            public GameActorCharmEffect charmEffect;
            public bool AppliesFreeze;
            public float FreezeApplyChance;
            public GameActorFreezeEffect freezeEffect;
            public bool AppliesFire;
            public float FireApplyChance;
            public GameActorFireEffect fireEffect;
            public bool AppliesStun;
            public float StunApplyChance;
            public float AppliedStunDuration;
            public bool AppliesBleed;
            public GameActorBleedEffect bleedEffect;
            public bool AppliesCheese;
            public float CheeseApplyChance;
            public GameActorCheeseEffect cheeseEffect;
            public float BleedApplyChance;
            public bool CanTransmogrify;
            public float ChanceToTransmogrify;
            public string[] TransmogrifyTargetGuids;
            public float BossDamageMultiplier;
            public bool SpawnedFromNonChallengeItem;
            public bool TreatedAsNonProjectileForChallenge;
            //public ProjectileImpactVFXPool hitEffects;
            public bool CenterTilemapHitEffectsByProjectileVelocity;
            //public VFXPool wallDecals;
            public bool damagesWalls;
            public float persistTime;
            public float angularVelocity;
            public float angularVelocityVariance;
            public string spawnEnemyGuidOnDeath;
            public bool HasFixedKnockbackDirection;
            public float FixedKnockbackDirection;
            public bool pierceMinorBreakables;
            public string objectImpactEventName;
            public string enemyImpactEventName;
            public string onDestroyEventName;
            public string additionalStartEventName;
            public bool IsRadialBurstLimited;
            public int MaxRadialBurstLimit;
            public SynergyBurstLimit[] AdditionalBurstLimits;
            public bool AppliesKnockbackToPlayer;
            public float PlayerKnockbackForce;
            public bool HasDefaultTint;
            public bool IsCritical;
            public float BlackPhantomDamageMultiplier;
            public bool PenetratesInternalWalls;
            public bool neverMaskThis;
            public bool isFakeBullet;
            public bool CanBecomeBlackBullet;
            public bool DelayedDamageToExploders;
            public bool IsBlackBullet;
            public List<GameActorEffect> statusEffectsToApply;
            public float AdditionalScaleMultiplier;
            public bool CurseSparks;
            //public ProjectileMotionModule OverrideMotionModule;
            public string OverrideMotionModuleType;

            public SerializableProjectile(Projectile orig)
            {
                SpawnedFromOtherPlayerProjectile = orig.SpawnedFromOtherPlayerProjectile;
                PlayerProjectileSourceGameTimeslice = orig.PlayerProjectileSourceGameTimeslice;
                BulletScriptSettings = orig.BulletScriptSettings;
                damageTypes = orig.damageTypes;
                allowSelfShooting = orig.allowSelfShooting;
                collidesWithPlayer = orig.collidesWithPlayer;
                collidesWithProjectiles = orig.collidesWithProjectiles;
                collidesOnlyWithPlayerProjectiles = orig.collidesOnlyWithPlayerProjectiles;
                projectileHitHealth = orig.projectileHitHealth;
                collidesWithEnemies = orig.collidesWithEnemies;
                shouldRotate = orig.shouldRotate;
                shouldFlipVertically = orig.shouldFlipVertically;
                shouldFlipHorizontally = orig.shouldFlipHorizontally;
                ignoreDamageCaps = orig.ignoreDamageCaps;
                baseData = orig.baseData;
                AppliesPoison = orig.AppliesPoison;
                PoisonApplyChance = orig.PoisonApplyChance;
                healthEffect = orig.healthEffect;
                AppliesSpeedModifier = orig.AppliesSpeedModifier;
                SpeedApplyChance = orig.SpeedApplyChance;
                speedEffect = orig.speedEffect;
                AppliesCharm = orig.AppliesCharm;
                CharmApplyChance = orig.CharmApplyChance;
                charmEffect = orig.charmEffect;
                AppliesFreeze = orig.AppliesFreeze;
                FreezeApplyChance = orig.FreezeApplyChance;
                freezeEffect = orig.freezeEffect;
                AppliesFire = orig.AppliesFire;
                FireApplyChance = orig.FireApplyChance;
                fireEffect = orig.fireEffect;
                AppliesStun = orig.AppliesStun;
                StunApplyChance = orig.StunApplyChance;
                AppliedStunDuration = orig.AppliedStunDuration;
                AppliesBleed = orig.AppliesBleed;
                bleedEffect = orig.bleedEffect;
                AppliesCheese = orig.AppliesCheese;
                CheeseApplyChance = orig.CheeseApplyChance;
                cheeseEffect = orig.cheeseEffect;
                BleedApplyChance = orig.BleedApplyChance;
                CanTransmogrify = orig.CanTransmogrify;
                ChanceToTransmogrify = orig.ChanceToTransmogrify;
                TransmogrifyTargetGuids = orig.TransmogrifyTargetGuids;
                BossDamageMultiplier = orig.BossDamageMultiplier;
                SpawnedFromNonChallengeItem = orig.SpawnedFromNonChallengeItem;
                TreatedAsNonProjectileForChallenge = orig.TreatedAsNonProjectileForChallenge;
                //hitEffects = orig.hitEffects;
                CenterTilemapHitEffectsByProjectileVelocity = orig.CenterTilemapHitEffectsByProjectileVelocity;
                //wallDecals = orig.wallDecals;
                damagesWalls = orig.damagesWalls;
                persistTime = orig.persistTime;
                angularVelocity = orig.angularVelocity;
                angularVelocityVariance = orig.angularVelocityVariance;
                spawnEnemyGuidOnDeath = orig.spawnEnemyGuidOnDeath;
                HasFixedKnockbackDirection = orig.HasFixedKnockbackDirection;
                FixedKnockbackDirection = orig.FixedKnockbackDirection;
                pierceMinorBreakables = orig.pierceMinorBreakables;
                objectImpactEventName = orig.objectImpactEventName;
                enemyImpactEventName = orig.enemyImpactEventName;
                onDestroyEventName = orig.onDestroyEventName;
                additionalStartEventName = orig.additionalStartEventName;
                IsRadialBurstLimited = orig.IsRadialBurstLimited;
                MaxRadialBurstLimit = orig.MaxRadialBurstLimit;
                AdditionalBurstLimits = orig.AdditionalBurstLimits;
                AppliesKnockbackToPlayer = orig.AppliesKnockbackToPlayer;
                PlayerKnockbackForce = orig.PlayerKnockbackForce;
                HasDefaultTint = orig.HasDefaultTint;
                //DefaultTintColor = orig.DefaultTintColor;
                IsCritical = orig.IsCritical;
                BlackPhantomDamageMultiplier = orig.BlackPhantomDamageMultiplier;
                PenetratesInternalWalls = orig.PenetratesInternalWalls;
                neverMaskThis = orig.neverMaskThis;
                isFakeBullet = orig.isFakeBullet;
                CanBecomeBlackBullet = orig.CanBecomeBlackBullet;
                //TrailRenderer = orig.TrailRenderer;
                //CustomTrailRenderer = orig.CustomTrailRenderer;
                //ParticleTrail = orig.ParticleTrail;
                DelayedDamageToExploders = orig.DelayedDamageToExploders;
                IsBlackBullet = orig.IsBlackBullet;
                statusEffectsToApply = orig.statusEffectsToApply;
                AdditionalScaleMultiplier = orig.AdditionalScaleMultiplier;
                CurseSparks = orig.CurseSparks;
                //OverrideMotionModule = orig.OverrideMotionModule;
                OverrideMotionModuleType = orig.OverrideMotionModule?.GetType().ToString();
            }
        }

        public class SerializableChargeProjectile
        {
            public float ChargeTime;
            public SerializableProjectile Projectile;
            public ProjectileModule.ChargeProjectileProperties UsedProperties;
            public int AmmoCost;
            //public VFXPool VfxPool;
            public float LightIntensity;
            public ScreenShakeSettings ScreenShake;
            public string OverrideShootAnimation;
            //public VFXPool OverrideMuzzleFlashVfxPool;
            public bool MegaReflection;
            public string AdditionalWwiseEvent;

            public SerializableChargeProjectile(ProjectileModule.ChargeProjectile orig)
            {
                ChargeTime = orig.ChargeTime;
                if(orig.Projectile != null)
                    Projectile = new SerializableProjectile(orig.Projectile);
                UsedProperties = orig.UsedProperties;
                AmmoCost = orig.AmmoCost;
                LightIntensity = orig.LightIntensity;
                ScreenShake = orig.ScreenShake;
                OverrideShootAnimation = orig.OverrideShootAnimation;
                MegaReflection = orig.MegaReflection;
                AdditionalWwiseEvent = orig.AdditionalWwiseEvent;
            }
        }

        public class SerializableProjectileModule
        {
            public ProjectileModule.ShootStyle shootStyle;
            public GameUIAmmoType.AmmoType ammoType;
            public string customAmmoType;
            public List<SerializableProjectile> projectiles;
            public ProjectileModule.ProjectileSequenceStyle sequenceStyle;
            public List<int> orderedGroupCounts;
            public float maxChargeTime;
            public bool triggerCooldownForAnyChargeAmount;
            public bool isFinalVolley;
            public bool usesOptionalFinalProjectile;
            public SerializableProjectile finalProjectile;
            public SerializableVolley finalVolley;
            public int numberOfFinalProjectiles;
            public GameUIAmmoType.AmmoType finalAmmoType;
            public string finalCustomAmmoType;
            public float angleFromAim;
            public bool alternateAngle;
            public float angleVariance;
            public bool mirror;
            public bool inverted;
            public int ammoCost;
            public int burstShotCount;
            public float burstCooldownTime;
            public float cooldownTime;
            public int numberOfShotsInClip;
            public bool ignoredForReloadPurposes;
            public bool preventFiringDuringCharge;
            public bool isExternalAddedModule;
            public string runtimeGuid;
            public bool IsDuctTapeModule;

            public List<SerializableChargeProjectile> chargeProjectiles;

            public SerializableProjectileModule(ProjectileModule orig)
            {
                if (orig.chargeProjectiles != null)
                {
                    chargeProjectiles = new List<SerializableChargeProjectile>();
                    foreach (var v in orig.chargeProjectiles)
                    {
                        chargeProjectiles.Add(new SerializableChargeProjectile(v));
                    }
                }

                shootStyle = orig.shootStyle;
                ammoType = orig.ammoType;
                customAmmoType = orig.customAmmoType;
                sequenceStyle = orig.sequenceStyle;
                orderedGroupCounts = orig.orderedGroupCounts;
                //chargeProjectiles = orig.chargeProjectiles;
                maxChargeTime = orig.maxChargeTime;
                triggerCooldownForAnyChargeAmount = orig.triggerCooldownForAnyChargeAmount;
                isFinalVolley = orig.isFinalVolley;
                usesOptionalFinalProjectile = orig.usesOptionalFinalProjectile;
                if(orig.finalProjectile != null)
                    finalProjectile = new SerializableProjectile(orig.finalProjectile);
                if(orig.finalVolley != null)
                    finalVolley = new SerializableVolley(orig.finalVolley);
                numberOfFinalProjectiles = orig.numberOfFinalProjectiles;
                finalAmmoType = orig.finalAmmoType;
                finalCustomAmmoType = orig.finalCustomAmmoType;
                angleFromAim = orig.angleFromAim;
                alternateAngle = orig.alternateAngle;
                angleVariance = orig.angleVariance;
                mirror = orig.mirror;
                inverted = orig.inverted;
                ammoCost = orig.ammoCost;
                burstShotCount = orig.burstShotCount;
                burstCooldownTime = orig.burstCooldownTime;
                cooldownTime = orig.cooldownTime;
                numberOfShotsInClip = orig.numberOfShotsInClip;
                ignoredForReloadPurposes = orig.ignoredForReloadPurposes;
                preventFiringDuringCharge = orig.preventFiringDuringCharge;
                isExternalAddedModule = orig.isExternalAddedModule;
                runtimeGuid = orig.runtimeGuid;
                IsDuctTapeModule = orig.IsDuctTapeModule;
                
                if (orig.projectiles != null)
                {
                    projectiles = new List<SerializableProjectile>();
                    foreach (var v in orig.projectiles)
                    {
                        projectiles.Add(v != null ? new SerializableProjectile(v) : null);
                    }
                }
            }
        }

        public class SerializableVolley
        {
            public bool UsesBeamRotationLimiter;
            public float BeamRotationDegreesPerSecond;
            public bool ModulesAreTiers;
            public bool UsesShotgunStyleVelocityRandomizer;
            public float DecreaseFinalSpeedPercentMin;
            public float IncreaseFinalSpeedPercentMax;

            public List<SerializableProjectileModule> Projectiles;

            public SerializableVolley(ProjectileVolleyData orig)
            {
                //UnityEngine.Debug.Log(orig);

                UsesBeamRotationLimiter = orig.UsesBeamRotationLimiter;
                BeamRotationDegreesPerSecond = orig.BeamRotationDegreesPerSecond;
                ModulesAreTiers = orig.ModulesAreTiers;
                UsesShotgunStyleVelocityRandomizer = orig.UsesShotgunStyleVelocityRandomizer;
                DecreaseFinalSpeedPercentMin = orig.DecreaseFinalSpeedPercentMin;
                IncreaseFinalSpeedPercentMax = orig.IncreaseFinalSpeedPercentMax;

                if (orig.projectiles != null)
                {
                    Projectiles = new List<SerializableProjectileModule>();
                    foreach (var v in orig.projectiles)
                    {
                        Projectiles.Add(new SerializableProjectileModule(v));
                    }
                }
            }
        }

        class GunEntry : PickupObjectEntry
        {
            public int MaxAmmo;
            //public int AdditionalClipCapacity;
            public bool CanAttackThroughObjects;
            public bool CanCriticalFire;
            public bool CanGainAmmo;
            public bool CanReloadNoMatterAmmo;
            public bool CanSneakAttack;
            public int DamageModifier;
            public bool InfiniteAmmo;
            public float ReloadTime;
            public bool IsAutomatic;
            public int ClipSize;
            public GameUIAmmoType.AmmoType AmmoType;
            public bool Unswitchable;
            public ProjectileModule.ShootStyle ShootStyle;
            public SerializableVolley Volley;
            public SerializableProjectileModule SingleModule;

            public override string EntryType
            {
                get => "Gun";
            }

            protected override void SetupInwards()
            {
                if (Name == null) Name = (origvalue as Gun).gunName;
                base.SetupInwards();
            }

            public GunEntry(PickupObject item) : base(item) { }
            public GunEntry(Gun item) : base(item)
            {
                origvalue = item;

                MaxAmmo = item.AdjustedMaxAmmo;
                CanAttackThroughObjects = item.CanAttackThroughObjects;
                CanCriticalFire = item.CanCriticalFire;
                CanGainAmmo = item.CanGainAmmo;
                CanReloadNoMatterAmmo = item.CanReloadNoMatterAmmo;
                CanSneakAttack = item.CanSneakAttack;
                DamageModifier = item.damageModifier;
                InfiniteAmmo = item.InfiniteAmmo;
                ReloadTime = item.reloadTime;
                IsAutomatic = item.IsAutomatic;
                ClipSize = item.ClipCapacity;
                AmmoType = item.AmmoType;
                Unswitchable = item.UnswitchableGun;
                ShootStyle = item.DefaultModule.shootStyle;
                SingleModule = new SerializableProjectileModule(item.singleModule);

                if(item.Volley != null)
                    Volley = new SerializableVolley(item.Volley);

                //EncounterTrackable encounter = item.GetComponent<EncounterTrackable>();

                //if (encounter != null)
                //{
                //    SetupEncounterData(EncounterDatabase.GetEntry(encounter.EncounterGuid));
                //}
            }
        }
    }
}
