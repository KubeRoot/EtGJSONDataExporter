﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EtGJSONDataExporter
{
    public partial class DatabaseHandler
    {
        class ItemEntry : PickupObjectEntry
        {
            public bool IsPlayerItem;

            public override string EntryType
            {
                get => "Item";
            }

            public ItemEntry(PickupObject item) : base(item)
            {
                IsPlayerItem = item is PlayerItem;
            }
        }

        class PassiveItemEntry : ItemEntry
        {
            public override string EntryType
            {
                get => "PassiveItem";
            }

            public PassiveItemEntry(PickupObject item) : base(item) { }
            public PassiveItemEntry(PassiveItem item) : base(item)
            {
            }

        }

        class PlayerItemEntry : ItemEntry
        {
            public bool CanStack;
            public bool Consumable;
            public float DamageCooldown;
            public int NumberOfUses;
            public int RoomCooldown;
            public float TimeCooldown;
            public bool UsableDuringDodgeroll;
            public bool UsesNumberOfUsesBeforeCooldown;

            public override string EntryType
            {
                get => "PlayerItem";
            }

            public PlayerItemEntry(PickupObject item) : base(item) { }
            public PlayerItemEntry(PlayerItem item) : base(item)
            {
                CanStack = item.canStack;
                Consumable = item.consumable;
                DamageCooldown = item.damageCooldown;
                NumberOfUses = item.numberOfUses;
                RoomCooldown = item.roomCooldown;
                TimeCooldown = item.timeCooldown;
                UsableDuringDodgeroll = item.usableDuringDodgeRoll;
                UsesNumberOfUsesBeforeCooldown = item.UsesNumberOfUsesBeforeCooldown;
            }

        }

    }
}
