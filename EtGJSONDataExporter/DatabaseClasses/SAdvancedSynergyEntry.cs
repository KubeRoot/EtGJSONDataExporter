﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EtGJSONDataExporter
{
    public partial class DatabaseHandler
    {
        class SAdvancedSynergyEntry : ParsedEntry
        {
            //public string Name;
            public AdvancedSynergyEntry Data;

            public override string EntryType
            {
                get => "AdvancedSynergy";
            }

            public SAdvancedSynergyEntry(AdvancedSynergyEntry entry) : base(entry)
            {
                Data = entry;
                Name = StringTableManager.GetSynergyString(Data.NameKey);
            }
        }
    }
}
