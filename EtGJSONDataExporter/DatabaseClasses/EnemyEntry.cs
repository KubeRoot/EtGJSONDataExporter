﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EtGJSONDataExporter
{
    public partial class DatabaseHandler
    {
        class EnemyEntry : ParsedEntry
        {
            public bool IsBoss;
            public bool IsNormalEnemy;
            public DungeonPlaceableBehaviour.PlaceableDifficulty difficulty;
            public string assetBundleGuid;

            public override string EntryType
            {
                get => "Enemy";
            }

            public EnemyEntry(EnemyDatabaseEntry enemy) : base(enemy)
            {
                IsBoss = enemy.isInBossTab;
                IsNormalEnemy = enemy.isNormalEnemy;
                Name = enemy.name;
                difficulty = enemy.difficulty;
                maybeuniquename = enemy.path;
                assetBundleGuid = enemy.myGuid;

                if (!string.IsNullOrEmpty(enemy.encounterGuid))
                {
                    SetupEncounterData(EncounterDatabase.GetEntry(enemy.encounterGuid));
                }

                if (Name == null) Name = enemy.path;
            }
        }

    }
}
