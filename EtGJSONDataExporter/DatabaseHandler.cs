﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace EtGJSONDataExporter
{
    public partial class DatabaseHandler
    {
        private static EncounterDatabase encounterdb = EncounterDatabase.Instance;
        private static tk2dSpriteCollectionData iconCollection = AmmonomiconController.ForceInstance.EncounterIconCollection;

        class ParsedEntry
        {
            public string Name
            {
                set
                {
                    if (value == null || value == "")
                    {
                        _name = null;
                        return;
                    }

                    if(!(value.Contains("NOT_FOUND")))
                    {
                        _name = value;
                    }
                }
                get
                {
                    return _name;
                }
            }
            public virtual string EntryType
            {
                get => "Generic";
            }
            private string _name = null;
            public string Description;
            public bool HasEncounterData = false;
            public string Base64PNGSprite;
            public string SpriteFlipMode;
            public SerializableVector2[] uvs = null;
            public string maybeuniquename;
            public SerializablePrerequisite[] Prerequisites;
            public string NotificationPanelDescription;

            protected object origvalue;
            protected virtual void SetupInwards()
            {

            }

            public ParsedEntry(object orig)
            {
                origvalue = orig;
                SetupInwards();
            }


            protected void SetupEncounterData(EncounterDatabaseEntry entry)
            {
                JournalEntry journaldata = entry.journalData;

                if (journaldata.IsEnemy != this is EnemyEntry)
                    return;

                HasEncounterData = true;

                Name = journaldata.GetPrimaryDisplayName();
                Description = entry.GetModifiedLongDescription();
                NotificationPanelDescription = journaldata.GetNotificationPanelDescription();
                //Prerequisites = entry.prerequisites;

                if(entry.prerequisites != null && entry.prerequisites.Length > 0)
                {
                    DungeonPrerequisite[] origprerequisites = entry.prerequisites;
                    Prerequisites = new SerializablePrerequisite[origprerequisites.Length];
                    for(int i = 0; i<origprerequisites.Length; i++)
                    {
                        Prerequisites[i] = new SerializablePrerequisite(origprerequisites[i]);
                    }
                }

                if (!string.IsNullOrEmpty(journaldata.AmmonomiconSprite))
                {
                    var spritedef = iconCollection.GetSpriteDefinition(journaldata.AmmonomiconSprite);
                    var texture = spritedef?.material?.mainTexture;
                    if(texture is Texture2D)
                    {
                        Vector2 pos0 = spritedef.uvs[0];
                        Vector2 diff = spritedef.uvs[3] - pos0;
                        uvs = new SerializableVector2[4];
                        for(int i = 0; i<4; i++)
                        {
                            uvs[i] = SerializableVector2.FromVector2(spritedef.uvs[i]);
                        }
                        //Debug.Log($"Creating texture for {Name}, pos0: {pos0}, diff: {diff}");
                        //Debug.Log($"{spritedef.uvs[0]} {spritedef.uvs[1]} {spritedef.uvs[2]} {spritedef.uvs[3]}");
                        int x = (int)Math.Round(pos0.x * texture.width), y = (int)Math.Round(pos0.y * texture.height);
                        int w = (int)Math.Round(diff.x * texture.width), h = (int)Math.Round(diff.y * texture.height);
                        //Debug.Log($"{x} {y} {w} {h}");
                        var origtexture2D = texture as Texture2D;
                        var newtexture2D = new Texture2D(w, h);
                        newtexture2D.SetPixels(origtexture2D.GetPixels(x, y, w, h));
                        Base64PNGSprite = System.Convert.ToBase64String(ImageConversion.EncodeToPNG(newtexture2D));
                        //if(maybeuniquename.Contains("Honeycomb"))
                        //{
                        //    System.IO.File.WriteAllBytes("test.png", ImageConversion.EncodeToPNG(newtexture2D));
                        //}
                        SpriteFlipMode = spritedef.flipped.ToString();
                    }
                }
            }

            public virtual bool ShouldAdd()
            {
                return true;
            }
        }
        
        class ParsedDatabase
        {
            public class PickupObjectsContainer
            {
                public List<GunEntry> Guns = new List<GunEntry>();
                public List<ItemEntry> Items = new List<ItemEntry>();
                public List<PickupObjectEntry> Other = new List<PickupObjectEntry>();
            }

            public PickupObjectsContainer PickupObjects = new PickupObjectsContainer();

            public List<EnemyEntry> Enemies = new List<EnemyEntry>();

            public SAdvancedSynergyEntry[] Synergies = null;
        }
        
        ParsedDatabase _parsed;

        public void Parse()
        {
            _parsed = new ParsedDatabase();

            foreach(var entry in PickupObjectDatabase.Instance.Objects)
            {
                if(entry is Gun)
                {
                    GunEntry parsed = new GunEntry(entry as Gun);
                    if(parsed.ShouldAdd())
                        _parsed.PickupObjects.Guns.Add(parsed);
                }
                else if(entry is PassiveItem)
                {
                    PassiveItemEntry parsed = new PassiveItemEntry(entry as PassiveItem);
                    if (parsed.ShouldAdd())
                        _parsed.PickupObjects.Items.Add(parsed);
                }
                else if(entry is PlayerItem)
                {
                    PlayerItemEntry parsed = new PlayerItemEntry(entry as PlayerItem);
                    if (parsed.ShouldAdd())
                        _parsed.PickupObjects.Items.Add(parsed);
                }
                else if(entry != null)
                {
                    PickupObjectEntry parsed = new PickupObjectEntry(entry);
                    if (parsed.ShouldAdd())
                        _parsed.PickupObjects.Other.Add(parsed);
                }
            }

            foreach (var entry in EnemyDatabase.Instance.Entries)
            {
                EnemyEntry parsed = new EnemyEntry(entry);
                if (parsed.ShouldAdd())
                    _parsed.Enemies.Add(parsed);
            }

            AdvancedSynergyEntry[] synergies = GameManager.Instance.SynergyManager.synergies;
            _parsed.Synergies = new SAdvancedSynergyEntry[synergies.Length];
            int j = 0;
            for(int i = 0; i<synergies.Length; i++)
            {
                SAdvancedSynergyEntry parsed = new SAdvancedSynergyEntry(synergies[i]);
                if (parsed.ShouldAdd())
                    _parsed.Synergies[j++] = parsed;
            }
        }

        private static JsonSerializerSettings serializerSettings;

        class ClassBlacklistResolver : DefaultContractResolver
        {
            public new static readonly ClassBlacklistResolver Instance = new ClassBlacklistResolver();

            private static HashSet<Type> blacklist =
            new HashSet<Type>{
                typeof(Color), typeof(Vector2), typeof(Vector3), typeof(GameObject), typeof(GoopDefinition), typeof(List<GameObject>)
            };

            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                JsonProperty property = base.CreateProperty(member, memberSerialization);

                if (blacklist.Contains(property.DeclaringType))
                {
                    property.ShouldSerialize =
                        instance => false;
                }

                return property;
            }
        }

        public string GetJSONRepresentation(bool pretty = false)
        {
            if (_parsed == null) Parse();

            if(serializerSettings == null)
            {
                serializerSettings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    ContractResolver = ClassBlacklistResolver.Instance,
                };

                serializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter(false));
            }

            return JsonConvert.SerializeObject(_parsed, pretty ? Formatting.Indented : Formatting.None, serializerSettings);
        }

        public void Cleanup()
        {
            _parsed = null;
        }
    }
}