﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace EtGJSONDataExporter
{
    public static class Main
    {
        public static void DoStuff()
        {
            //Debug.Log("Starting to do stuff");
            DatabaseHandler dbhandler = new DatabaseHandler();
            //Debug.Log("Created handler");

            string json = dbhandler.GetJSONRepresentation(true);
            //Debug.Log("Got JSON text");
            System.IO.File.WriteAllText(@"DataDump.json", json);
            //Debug.Log("Wrote file");
            
            Application.Quit();
        }
    }
}
